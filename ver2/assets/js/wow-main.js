jQuery(document).ready(function() {
		jQuery('.promo-block_text-1-wrapper').addClass('wow fadeInDown').attr('data-wow-delay', '.8s');
		jQuery('.promo-block_text-2-wrapper').addClass('wow fadeIn').attr('data-wow-duration', '4s').attr('data-wow-delay', '1.8s');
		
		jQuery('.programma .cell:first-child ul.programma_list').addClass('wow fadeInLeft');
		jQuery('.programma .cell:last-child ul.programma_list').addClass('wow fadeInRight');

		jQuery('.infografika_title, .novosti_title, .video_title, .partners_title, .partners .grid-x:nth-child(2) .grid-x .cell, .video .grid-x:nth-child(2) .grid-x .cell:nth-child(3), .video .grid-x:nth-child(2) .grid-x .cell:nth-child(4), .video .grid-x:nth-child(2) .grid-x .cell:nth-child(5)').addClass('wow fadeIn').attr('data-wow-duration', '1s');
		
		jQuery('.infografika_title, .infografika .grid-x .large-4:first-child, .infografika .grid-x .large-4:last-child').addClass('wow-disable-mobile');
		
		jQuery('.infografika .grid-x .large-4:first-child, .novosti .grid-x .xlarge-4:first-child, .days-start .grid-x:nth-child(2) .xlarge-4:nth-child(2), .days-start .grid-x:nth-child(3) .xlarge-4:first-child, .intro .grid-x .xlarge-4:first-child, .video .grid-x:nth-child(2) .grid-x .cell:nth-child(1)').addClass('wow fadeInLeft').attr('data-wow-delay', '.2s');
		jQuery('.infografika .grid-x .large-4:last-child, .novosti .grid-x .xlarge-4:last-child, .days-start .grid-x:nth-child(2) .xlarge-4:first-child, .days-start .grid-x:nth-child(3) .xlarge-4:nth-child(2), .intro .grid-x .xlarge-4:last-child, .video .grid-x:nth-child(2) .grid-x .cell:nth-child(2)').addClass('wow fadeInRight').attr('data-wow-delay', '.2s');
		jQuery('.novosti_top-wrapper').addClass('wow fadeIn').attr('data-wow-duration', '1s').attr('data-wow-delay', '.2s');
		jQuery('.novosti .more-button, .video .more-button').parent().addClass('wow fadeIn').attr('data-wow-duration', '1s').attr('data-wow-delay', '1s');
		jQuery('.days-start_title').addClass('wow fadeInLeft').attr('data-wow-delay', '.2s');
		new WOW().init();
});