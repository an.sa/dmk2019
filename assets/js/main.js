window.stickyMenu = {
	active: false,
	init: function (){
		jQuery(document).on("scroll", function() {
			navHeight = jQuery('header').height();
			var scroolTop = jQuery(document).scrollTop();
			if( scroolTop > navHeight + 200 && !stickyMenu.active){
				stickyMenu.activate();
			} else if(scroolTop < '120' && stickyMenu.active){
				stickyMenu.deactivate();
			}
		});
	},
	activate: function (){
		jQuery('header').addClass('header-sticky').css('margin-top', '-150px').animate({'margin-top': 0}, 500);
		//jQuery('header').addClass('header-sticky');
		
		stickyMenu.active = true;
	},
	deactivate: function (){
		jQuery('header').removeClass('header-sticky');
		
		stickyMenu.active = false;
	}
}

jQuery(document).ready(function() {
	stickyMenu.init();
	
	jQuery('.nav-menu-mobile').on('click', function(event){
		jQuery('.nav-menu').toggleClass('active');
	});
	
	jQuery('.nav-menu').on('click', function(event){
		jQuery('.nav-menu').toggleClass('active');
	});
	
	jQuery("a[rel='m_PageScroll2id']").mPageScroll2id({
		offset:"#header"
	});
	
});