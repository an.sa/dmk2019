ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
            center: [55.770811,37.591189],
            zoom: 16
        }, {
            searchControlProvider: 'yandex#search'
        });

    myMap.geoObjects
        .add(new ymaps.Placemark([55.770811,37.591189], {
            balloonContent: 'Москва, ул. 2-я Брестская, 6'
        }, {
            preset: 'islands#dotIcon',
            iconColor: '#2e4b5e'
        }));
}
