<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml" lang="ru-RU"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Дни московской конкуренции</title>
    <link rel="icon" type="image/png" href="/wp-content/themes/dmk/assets/images/favicon-16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="/wp-content/themes/dmk/assets/images/favicon-32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/wp-content/themes/dmk/assets/images/favicon-96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="/wp-content/themes/dmk/assets/images/favicon-128.png" sizes="128x128">

    <link rel="stylesheet" href="./quiz_files/general.css">
    <link rel="stylesheet" href="./quiz_files/index.css">
    <!--script src="./quiz_files/general.js.Без названия"></script--><style type="text/css">* {}

        .page-header_logo {
            display: block !important;
            position: absolute !important;
            top: 10px !important;
            left: 62px !important;
            height: 60px !important;
            width: 178px !important;
            background: url(./logos/logo.svg) no-repeat !important;
            background-size: contain !important;
        }

    </style>

</head>
<body>
<div id="page-wrapper" class="page-wrapper">
    <div class="page-header_wrapper">
        <div class="page-header_fixed">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="cell">
                        <header class="page-header">
                            <a href="https://www.mos.ru/" class="page-header_logo-m" target="_blank"></a>
                            <a href="/" class="page-header_logo"></a>
                            <nav class="nav-menu">
                                <div id="menu-trigger" class="nav-menu_trigger hide-for-large"></div>
                                <ul id="page-menu" class="nav-menu_list">




                                    <li class="nav-menu_item"><a href="/" class="nav-menu_link">главная</a></li>
                                    <li class="nav-menu_item"><a href="#about" class="nav-menu_link">о проекте</a></li>
                                    <li class="nav-menu_item"><a href="#winners" class="nav-menu_link-close">победители</a></li>
                                    <li class="nav-menu_item"><a href="#news" class="nav-menu_link-close">пресса о нас</a></li>

                                    <li class="nav-menu_item"><a href="#partners" class="nav-menu_link">партнеры</a></li>

                                    <li class="nav-menu_item"><a href="#comands" class="nav-menu_link-close">участники</a></li>

                                    <li class="nav-menu_item"><a href="#contacts" class="nav-menu_link-close">контакты</a></li>
                                    <li class="socials-dub hide-for-large">
                                        <a href="https://www.facebook.com/dnikonkurencii/" class="socials_fb" target="_blank"></a>
                                        <a href="https://vk.com/dnikonkurencii" class="socials_vk" target="_blank"></a>
                                        <a href="https://www.instagram.com/mos_tender/" class="socials_ig" target="_blank"></a>
                                        <a href="https://www.youtube.com/channel/UCNcUOaCjtcPMUlkcCeHtoWw" class="socials_yt" target="_blank"></a>
                                    </li>
                                </ul>
                            </nav>
                            <div class="socials show-for-large">
                                <a href="https://www.facebook.com/dnikonkurencii/" class="socials_fb" target="_blank"></a>
                                <a href="https://vk.com/dnikonkurencii" class="socials_vk" target="_blank"></a>
                                <a href="https://www.instagram.com/mos_tender/" class="socials_ig" target="_blank"></a>
                                <a href="https://www.youtube.com/channel/UCNcUOaCjtcPMUlkcCeHtoWw" class="socials_yt" target="_blank"></a>
                            </div>
                        </header>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="quiz-head">
        <div class="grid-container">
            <div class="grid-x text-center">
                <div class="cell">
                    <span class="quiz-head_subtitle">ПЕРВЫЙ  В ИСТОРИИ</span>
                </div>
            </div>
            <div class="grid-x text-center">
                <div class="cell">
                    <img class="quiz-head_image" src="./quiz_files/DMK_kviz_gl-pix.svg" alt="alt text">
                </div>
            </div>
            <div class="grid-x text-center">
                <div class="cell">
                    <span class="quiz-head_title">МОТИВАТОР</span>
                </div>
            </div>
            <div class="grid-x text-center align-center">
                <div class="xlarge-10 cell">
                    <div class="quiz-head_line">
                        <span class="quiz-head_line-text">ДЛЯ СТУДЕНТОВ НА ТЕМУ КОНКУРЕНЦИИ</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <a id="about"></a>
    <div class="quiz-lead">
        <div class="grid-container fluid">
            <div class="grid-x align-center">
                <div class="xlarge-9 medium-10 small-11 cell text-center">
                    <div class="quiz-lead_text">В  интеллектуальной игре приняли участие 15 команд.<br />  
										Конкуренция на студенческом квизе на тему «Конкуренция сегодня» была нешуточной, но место для шуток все-таки нашлось, ведь вопросы были связаны с реальной жизнью, а тут без юмора и 
										<br />оптимизма никак!
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-container">
            <div class="grid-x align-center">
                <div class="cell xlarge-6 large-8 medium-10 small-12">
                    <div class="video_wrapper">
                        <div class="video_inner">
                            <a class="video_link-quiz" data-fancybox="" href="https://youtu.be/gK9ho2TQjvc?rel=0">
                                <img class="video_cover-quiz" src="./quiz_files/video-quiz.jpg">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="targets">
        <div class="grid-container">
            <div class="grid-x targets-tiles">
                <div class="cell">
                    <div class="grid-x">
                        <div class="cell large-3 medium-6 small-12 targets-tile-image">
                            <img src="./quiz_files/DMK_quiz_01.jpg" alt="alt">
                        </div>
                        <div class="cell large-3 medium-6 small-12 targets-tile-text">
                            <span>15</span>
                            <span>вузов</span>
                        </div>
                        <div class="cell large-3 medium-6 small-12 targets-tile-image">
                            <img src="./quiz_files/DMK_quiz_02.jpg" alt="alt">
                        </div>
                        <div class="cell large-3 medium-6 small-12 targets-tile-text">
                            <span>150</span>
                            <span>участников</span>
                        </div>
                    </div>
                </div>
                <div class="cell">
                    <div class="grid-x">
                        <div class="cell large-3 medium-6 small-12 targets-tile-text">
                            <span>4 часа</span>
                            <span>конкурентной борьбы</span>
                        </div>
                        <div class="cell large-3 medium-6 small-12 targets-tile-image">
                            <img src="./quiz_files/DMK_quiz_03.jpg" alt="alt">
                        </div>
                        <div class="cell large-3 medium-6 small-12 targets-tile-text">
                            <span>50</span>
                            <span>вопросов</span>
                        </div>
                        <div class="cell large-3 medium-6 small-12 targets-tile-image">
                            <img src="./quiz_files/DMK_quiz_04.jpg" alt="alt">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="spacer"></div>

    <a class="anchor" id="winners"></a>

    <div class="quiz-winners">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell auto">
                    <div class="partners_title">Победители</div>
                </div>
            </div>
            <div class="grid-x align-center">
                <div class="cell large-4 medium-6 small-12">
                    <div class="quiz-winners_frame">
                        <img src="./quiz_files/DMK_quiz_priz_01.jpg">
                    </div>
                </div>
                <div class="cell large-4 medium-6 small-12">
                    <div class="quiz-winners_frame">
                        <div class="quiz-winners_text-wrapper">
                            <span class="quiz-winners_place">1 место</span>
                            <span class="quiz-winners_name">Российский университет<br class="show-for-medium"> транспорта</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-x align-center tiny-flex-dir-row-reverse">
                <div class="cell large-4 medium-6 small-12">
                    <div class="quiz-winners_frame">
                        <img src="./quiz_files/DMK_quiz_priz_02.jpg">
                    </div>
                </div>
                <div class="cell large-4 medium-6 small-12">
                    <div class="quiz-winners_frame">
                        <div class="quiz-winners_text-wrapper">
                            <span class="quiz-winners_place">2 место</span>
                            <span class="quiz-winners_name">Российский новый<br class="show-for-medium"> университет</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-x align-center">
                <div class="cell large-4 medium-6 small-12">
                    <div class="quiz-winners_frame">
                        <img src="./quiz_files/DMK_quiz_priz_03.jpg">
                    </div>
                </div>
                <div class="cell large-4 medium-6 small-12">
                    <div class="quiz-winners_frame">
                        <div class="quiz-winners_text-wrapper">
                            <span class="quiz-winners_place">3 место</span>
                            <span class="quiz-winners_name">Московский государственный технический университет им.&nbsp;Н.&nbsp;Э.&nbsp;Баумана</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="quiz-about">
        <div class="grid-container">
            <div class="grid-x text-center align-center">
                <div class="xlarge-10 large-11 medium-12 cell">
                    <div class="quiz-about_wrapper">
                        <div class="quiz-about_question">Думаешь, что конкуренция – это только про экономику</div>
                        <div class="quiz-about_text">Наш квиз позволил в&nbsp;современном игровом формате доказать, что конкуренция встречается в&nbsp;нашей жизни каждый день, это эффективный инструмент развития и принятия решений</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="quiz-about-2">
        <div class="grid-container">
            <div class="grid-x align-center">
                <div class="xlarge-10 large-11 medium-12 cell">
                    <div class="grid-x grid-padding-x grid-padding-y text-center">
                        <div class="medium-6 small-12 cell small-order-1 ">
                            <div class="quiz-about-2_quiz">это командное состязание в формате «вопрос-ответ», рассчитанное не только на знания, но и на сообразительность игроков, их логику, быстроту реакции и чувство юмора.</div>
                        </div>
                        <div class="medium-6 small-12 cell small-order-2">
                            <img class="quiz-about-2_pic" src="./quiz_files/DMK_kviz_pix-8.svg" alt="alt text">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <a class="anchor" id="news"></a>
    <div class="novosti" style="padding-top: 80px;">
        <div class="grid-container">
            <div class="grid-x">
                <div class="cell">
                    <div class="novosti_title">Пресса о нас</div>
                </div>
            </div>



            <div class="grid-x align-center">
                <div class="xlarge-8 large-10 medium-12 small-12 cell">
                    <div class="novosti_top-wrapper">
                        <div class="grid-x grid-padding-x grid-padding-y">
                            <div class="medium-6 small-12 cell">
                                <div class="novosti_top-left">
                                    <span class="novosti_top-date">29 ноября 2019</span>
                                    <a target="_blank" class="novosti_top-title"
                                       href="https://russian.rt.com/nopolitics/news/691647-moskva-kviz-konkurenciya">RT: В Москве прошёл интеллектуальный квиз «Конкурент-мотиватор»​</a>
                                    <span class="novosti_top-excerpt">Более 100 студентов из 15 вузов Москвы стали участниками первого интеллектуального квиза «Конкурент-мотиватор», который прошёл в рамках проекта «Дни московской конкуренции» в столице, сообщает НСН.</span>
                                </div>
                            </div>
                            <div class="medium-6 small-12 cell">
                                <div class="novosti_top-right">
                                    <a target="_blank" class="novosti_top-image-link"
                                       href="https://russian.rt.com/nopolitics/news/691647-moskva-kviz-konkurenciya">
                                        <img src="http://xn--80aaenwcalncade4ah0an6t.xn--p1ai/news/wp-content/uploads/2019/11/IMG_9939.jpg"
                                             class="novosti_top-image"
                                             alt="alt text">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







            <div class="grid-x grid-padding-x grid-padding-y align-center">


                <div class="xlarge-4 large-5 medium-6 small-12 cell">



                    <span class="novosti_date">Малый Бизнес</span>
                    <a target="_blank" class="novosti_item-title"
                       href="http://mbgazeta.ru/news/15-vuzov-prinyali-uchastie-v-studencheskom-kvize-dnej-moskovskoj-konkurentsii/"> 15 вузов приняли участие в студенческом квизе Дней московской конкуренции </a>
                    <span class="novosti_excerpt">Более ста студентов из 15 вузов Москвы стали участниками первого интеллектуального квиза «Конкурент-мотиватор», который прошёл на площадке Агентства стратегических инициатив – «Точка кипения» в МИСиС.</span>


                </div>







                <div class="xlarge-4 large-5 medium-6 small-12 cell">



                    <span class="novosti_date">Вечерняя Москва</span>
                    <a target="_blank" class="novosti_item-title"
                       href="https://vm.ru/news/765868-nazvan-pobeditel-moskovskogo-studencheskogo-kviza-po-konkurencii"> Назван победитель московского студенческого квиза по конкуренции </a>
                    <span class="novosti_excerpt">Команда Российского университета транспорта заняла первое место в московском студенческом квизе «Конкурент-мотиватор», который 28 ноября впервые организовал Департамент города Москвы по конкурентной политике на столичной площадке агентства стратегических инициатив «Точка кипения» МИСиС.</span>


                </div>



            </div>









            <div class="grid-x grid-padding-x grid-padding-y align-center">


                <div class="xlarge-4 large-5 medium-6 small-12 cell">



                    <span class="novosti_date">НСН</span>
                    <a target="_blank" class="novosti_item-title"
                       href="https://nsn.fm/spetsproekty/bolee-sotni-studentov-sobral-kviz-dnei-moskovskoi-konkurentsii"> Более сотни студентов собрал квиз Дней московской конкуренции </a>
                    <span class="novosti_excerpt">Вузы с готовностью приняли приглашение принять участие в интеллектуальном квизе и на практике поняли, насколько многогранным является понятие конкуренции.</span>


                </div>







                <div class="xlarge-4 large-5 medium-6 small-12 cell">



                    <span class="novosti_date">Москва24</span>
                    <a target="_blank" class="novosti_item-title"
                       href="https://www.m24.ru/videos/gorod/28112019/221895"> Московские студенты участвуют в квизе «Конкурент-мотиватор» </a>
                    <span class="novosti_excerpt">Как устроена конкуренция и что делать, чтобы быть на волне трендов? Столичные студенты разберутся в сложных вопросах в формате интеллектуальной игры.</span>


                </div>



            </div>







            <!--div class="grid-x">
                <div class="cell text-center">
                    <a class="more-button" href="#">посмотреть ещё</a>
                </div>
            </div-->
        </div>
    </div>
    <!--div class="itogovoe">
        <div class="grid-container">
            <div class="grid-x align-center">
                <div class="cell xlarge-8 large-10 medium-12">
                    <h2 class="itogovoe_title"><span>18 декабря</span> итоговое мероприятие</h2>
                    <!--div><a href="#" class="itogovoe_button">Хочу попасть</a></div-->
</div>
</div>
</div>
</div-->

    <div class="margin-bottom-collapse"></div>




    <!--div class="partners">
        <div class="partners_wrapper">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="cell">
                        <div class="partners_title">ПАРТНЕРЫ</div>
                    </div>
                </div>
                <div class="grid-x grid-padding-x grid-padding-y align-center">
                    <div class="medium-12 large-10 xlarge-8 cell">
                        <div class="grid-x grid-padding-x grid-padding-y align-center text-center">
                            <div class="large-2 medium-3 small-6 cell">
                                <a href="https://www.mos.ru/tender/"><img src="./quiz_files/2(1).jpg" alt=""></a>
                            </div>
                            <div class="large-2 medium-3 small-6 cell">
                                <a href="https://www.mos.ru/depr/"><img src="./quiz_files/3.jpg" alt=""></a>
                            </div>
                            <div class="large-2 medium-3 small-6 cell">
                                <a href="https://www.mos.ru/dit/"><img src="./quiz_files/4.jpg" alt=""></a>
                            </div>
                            <div class="large-2 medium-3 small-6 cell">
                                <a href="https://fas.gov.ru/"><img src="./quiz_files/5.jpg" alt=""></a>
                            </div>
                            <div class="large-2 medium-3 small-6 cell">
                                <a href="https://www.mos.ru/dipp/"><img src="./quiz_files/dipp.png" alt=""></a>
                            </div>
                            <div class="large-2 medium-3 small-6 cell">
                                <a href="https://www.minfin.ru/ru/"><img src="./quiz_files/6.jpg" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div-->
    <div class="margin-bottom-collapse"></div>


    <a class="anchor" id="partners"></a>
    <div class="partners">
        <div class="partners_wrapper">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="cell">
                        <div class="partners_title">ПАРТНЕРЫ</div>
                    </div>
                </div>
                <div class="grid-x grid-padding-x grid-padding-y align-center">
                    <div class="medium-12 large-10 xlarge-8 cell">
                        
												<div class="grid-x grid-padding-x grid-padding-y align-center text-center">
													<div class="large-4 medium-3 small-6 cell">

                                <a href="https://skillbox.ru/" target="_blank">
                                    <img
                                            src="/assets/images/quiz/4.png"
                                            alt="">
                                </a>
                            </div>
                            <div class="large-4 medium-3 small-6 cell">

                                <a href="https://mozgva.com/" target="_blank">
                                    <img src="/assets/images/quiz/3.png" alt="">
                                </a>
                            </div>
														<div class="large-4 medium-3 small-6 cell">

                                <a href="https://bombora.ru/" target="_blank">
                                    <img src="/assets/images/quiz/2.png" alt="">
                                </a>
                            </div>
														<div class="large-4 medium-3 small-6 cell">

                                <a href="https://z-store.ru/" target="_blank">
                                    <img
                                            src="/assets/images/quiz/1.png"
                                            alt="">
                                </a>
                            </div>




                        </div>
												
                    </div>
                </div>
            </div>
        </div>
        </div>
<a class="anchor" id="comands"></a>
<div class="participants">
    <div class="grid-container">
        <div class="grid-x grid-padding-y">
            <div class="cell">
                <div class="partners_title">участники</div>
            </div>
        </div>
        <div class="grid-x grid-padding-x align-center">
            <div class="large-5 medium-6 small-12 cell">
                <div class="participants_list-left">
                    <div class="participants_list-item">
                        <span class="participants_item-number">1.</span><span class="participants_item-text">Росийский университет дружбы народов</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">2.</span><span class="participants_item-text">Российский университет транспорта</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">3.</span><span class="participants_item-text">Финансовый университет при правительстве РФ</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">4.</span><span class="participants_item-text">Московский городской педагогический университет</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">5.</span><span class="participants_item-text">Московский государственный технический университет им. Н.Э. Баумана</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">6.</span><span class="participants_item-text">Московский автомобильно-дорожный государственный технический университет</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">7.</span><span class="participants_item-text">Российский новый университет</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">8.</span><span class="participants_item-text">Московский государственный юридический университет им. О.Е. Кутафина</span>
                    </div>
                </div>
            </div>
            <div class="large-5 medium-6 small-12 cell">
                <div class="participants_list-right">
                    <div class="participants_list-item">
                        <span class="participants_item-number">9.</span><span class="participants_item-text">Государственный университет управления</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">10.</span><span class="participants_item-text">Национальный исследовательский ядерный университет «МИФИ»</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">11.</span><span class="participants_item-text">Российский государственный социальный университет</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">12.</span><span class="participants_item-text">Московская финансовая юридическая академия</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">13.</span><span class="participants_item-text">Российский государственный геологоразведочный университет имени Серго Орджоникидзе</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">14.</span><span class="participants_item-text">Московский городской университет управления Правительства Москвы</span>
                    </div>
                    <div class="participants_list-item">
                        <span class="participants_item-number">15.</span><span class="participants_item-text">Московский педагогический государственный университет</span>
                    </div>
                </div>
            </div>
        </div>


        <div class="grid-x grid-padding-x grid-padding-y align-center">
            <div class="medium-12 large-10 xlarge-8 cell">
                <div class="grid-x grid-padding-x grid-padding-y align-center text-center">
                    <?php for($k=1;$k<=15;$k++):?>
                        <div class="large-2 medium-3 small-6 cell">
                            <a href=""><img src="<?php $nn=$k; if ($k<10) $nn="0$k";echo "./logos/log_uch_$nn.jpg";?>" alt=""></a>
                        </div>
                    <?php endfor;?>

                </div>
            </div>
        </div>

    </div>
</div>

<a class="anchor" id="contacts"></a>
<div class="contacts">
    <div class="grid-container">
        <div class="grid-x align-center">
            <div class="xlarge-10 large-11 medium-12 cell">
                <div class="contacts_inner">
                    <div class="contacts_title">КОНТАКТЫ</div>
                    <div class="grid-x grid-padding-x grid-padding-y align-center">
                        <div class="medium-5 small-10 cell medium-offset-1">
                            <div class="contacts_contact">
                                <span class="contacts_name">по общим вопросам:</span>
                                <span class="contacts_tel">+7 (495) 957-99-77</span>
                                <a class="contacts_email" href="mailto:mostender@mos.ru">mostender@mos.ru</a>
                            </div>
                        </div>
                        <div class="medium-5 small-10 cell medium-offset-1">
                            <div class="contacts_contact">
                                <span class="contacts_name yellow">для представителей СМИ:</span>
                                <span class="contacts_tel">+7 (495) 957-75-00, доб. 57-068</span>
                                <a class="contacts_email" href="mailto:pr-tender@mos.ru">pr-tender@mos.ru</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div></div>




</body></html>